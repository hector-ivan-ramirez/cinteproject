package col.humanos.cidenet.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "detalle_cliente")
public class DetalleCliente implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DET_CLI_SEQ")
  @SequenceGenerator(name="DET_CLI_SEQ",sequenceName = "detalle_cliente_id_seq",allocationSize=1)
  @Column(name = "id", nullable = false)
  private BigInteger id;

  @Column(name = "tipo_registro")
  private String tipoRegistro;

  @Column(name = "modalidad_planilla")
  private String modalidadPlanilla;

  @Column(name = "secuencia")
  private String secuencia;

  @Column(name = "tipo_documento")
  private String tipoDocumento;

  @Column(name = "nro_identificacion_documento")
  private String nroIdentificacionDocumento;

  @Column(name = "tipo_cotizante")
  private String tipoCotizante;

  @Column(name = "subtipo_cotizante")
  private String subtipoCotizante;

  @Column(name = "extranjero_cotizante")
  private String extranjeroCotizante;

  @Column(name = "colombiano_residente_ext")
  private String colombianoResExterior;

  @Column(name = "codigo_departamento")
  private String codigoDepartamento;

  @Column(name = "codigo_municipio")
  private String codigoMunicipio;

  @Column(name = "primer_apellido")
  private String primerApellido;

  @Column(name = "segundo_apellido")
  private String segundoApellido;

  @Column(name = "primer_nombre")
  private String primerNombre;

  @Column(name = "segundo_nombre")
  private String segundoNombre;

  @Column(name = "id_encabezado")
  private BigInteger idEncabezado;

}
