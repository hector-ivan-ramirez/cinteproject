package col.humanos.cidenet.model;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "encabezado_cliente")
public class EncabezadoCliente implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enc_seq")
  @SequenceGenerator(name="enc_seq", sequenceName = "encabezado_cliente_id_seq", initialValue = 1,allocationSize = 1)
  @Column(name = "id", nullable = false)
  private BigInteger id;

  @Column(name = "modalidad_planilla")
  private String modalidadPlanilla;

  @Column(name = "secuencia")
  private String secuencia;

  @Column(name = "tipo_registro")
  private String tipoRegistro;

  @Column(name = "nombre_razon_social")
  private String nombreRazonSocial;

  @Column(name = "nro_identificacion_aportante")
  private String nroIdentificacionAportantes;

  @Column(name = "digito_verificacion")
  private String digitoVerificacion;

  @Column(name = "tipo_documento")
  private String tipoDocumento;

}
