package col.humanos.cidenet.impl;


import col.humanos.cidenet.custom.EncabezadoClienteRepositoryCustom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class EncabezadoClienteRepositoryImpl implements EncabezadoClienteRepositoryCustom {

  public EncabezadoClienteRepositoryImpl() {
    // No es necesario el metodo.
  }

  // Inicializamos el sistema de log
  private static final Logger LOGGER = LoggerFactory.getLogger(EncabezadoClienteRepositoryImpl.class);

  @PersistenceContext
  private EntityManager em;
}