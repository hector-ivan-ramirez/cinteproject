package col.humanos.cidenet;

import col.humanos.cidenet.custom.EncabezadoClienteRepositoryCustom;
import col.humanos.cidenet.model.EncabezadoCliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface EncabezadoClienteRepository extends JpaRepository<EncabezadoCliente, BigInteger>, EncabezadoClienteRepositoryCustom {

}
