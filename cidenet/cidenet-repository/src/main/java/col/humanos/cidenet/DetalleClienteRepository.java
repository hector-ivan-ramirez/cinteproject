package col.humanos.cidenet;

import col.humanos.cidenet.model.DetalleCliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface DetalleClienteRepository extends JpaRepository<DetalleCliente, BigInteger> {

}
