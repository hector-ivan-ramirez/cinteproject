package col.humanos.cidenet;

import col.humanos.cidenet.model.EncabezadoCliente;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface EncabezadoClienteService {

  @Transactional
  EncabezadoCliente create(String linea, EncabezadoCliente encabezadoCliente);

}
