package col.humanos.cidenet.controller;

import col.humanos.cidenet.DetalleClienteService;
import col.humanos.cidenet.EncabezadoClienteService;
import col.humanos.cidenet.model.DetalleCliente;
import col.humanos.cidenet.model.EncabezadoCliente;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;


@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping(value="/api")
public class CargueClientesController {

  private EncabezadoClienteService encabezadoClienteService;
  private DetalleClienteService detalleClienteService;

  @Autowired
  public CargueClientesController(EncabezadoClienteService encabezadoClienteService,DetalleClienteService detalleClienteService) {
    this.encabezadoClienteService=encabezadoClienteService;
    this.detalleClienteService=detalleClienteService;
  }


  @PostMapping("/upload")
  public ResponseEntity<String> subirArchivo(@RequestParam("imageFile") MultipartFile file) throws IOException {


    BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-8"));

    String line;
    EncabezadoCliente encabezadoCliente=new EncabezadoCliente();
    while ((line = br.readLine()) != null) {

      String tipoRegistro=line.substring(0,2);
      if(StringUtils.equals(tipoRegistro,"01")){
        encabezadoCliente= new EncabezadoCliente();
        encabezadoCliente.setTipoRegistro(tipoRegistro);
        encabezadoClienteService.create(line,encabezadoCliente);
      }else{
        DetalleCliente detalleCliente= new DetalleCliente();
        detalleCliente.setTipoRegistro(tipoRegistro);
        detalleCliente.setIdEncabezado(encabezadoCliente.getId());
        detalleClienteService.create(line,detalleCliente);
      }
      System.out.println(line);
    }

    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping("clientes_detalle")
  public List<DetalleCliente> detalleCLientes() {
    return detalleClienteService.busquedaAvanzada();
  }


}
