package col.humanos.cidenet.serviceimpl;

import col.humanos.cidenet.DetalleClienteRepository;
import col.humanos.cidenet.DetalleClienteService;
import col.humanos.cidenet.EncabezadoClienteRepository;
import col.humanos.cidenet.EncabezadoClienteService;
import col.humanos.cidenet.model.DetalleCliente;
import col.humanos.cidenet.model.EncabezadoCliente;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DetalleClienteServiceImpl implements DetalleClienteService {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = LoggerFactory.getLogger(DetalleClienteServiceImpl.class);

  private DetalleClienteRepository detalleClienteRepository;

  @Autowired
  public DetalleClienteServiceImpl(DetalleClienteRepository detalleClienteRepository) {
    this.detalleClienteRepository=detalleClienteRepository;
  }


  @Override
  public DetalleCliente create(String linea, DetalleCliente detalleCliente) {
    configurarDetalle(linea,detalleCliente);
    return detalleClienteRepository.save(detalleCliente);
  }

  private DetalleCliente configurarDetalle(String linea,DetalleCliente detalleCliente){
    detalleCliente.setModalidadPlanilla(StringUtils.substring(linea,2,3));
    detalleCliente.setSecuencia(StringUtils.substring(linea,3,7));
    detalleCliente.setTipoDocumento(StringUtils.substring(linea,7,9));
    detalleCliente.setNroIdentificacionDocumento(StringUtils.substring(linea,9,25));
    detalleCliente.setTipoCotizante(StringUtils.substring(linea,25,27));
    detalleCliente.setSubtipoCotizante(StringUtils.substring(linea,27,29));
    detalleCliente.setExtranjeroCotizante(StringUtils.substring(linea,29,30));
    detalleCliente.setColombianoResExterior(StringUtils.substring(linea,30,31));
    detalleCliente.setCodigoDepartamento(StringUtils.substring(linea,31,33));
    detalleCliente.setCodigoMunicipio(StringUtils.substring(linea,33,36));
    detalleCliente.setPrimerApellido(StringUtils.substring(linea,36,56).trim());
    detalleCliente.setSegundoApellido(StringUtils.substring(linea,56,86).trim());
    detalleCliente.setPrimerNombre(StringUtils.substring(linea,86,106).trim());
    detalleCliente.setSegundoNombre(StringUtils.substring(linea,106,136).trim());

    return detalleCliente;
  }


  @Override
  public List<DetalleCliente> busquedaAvanzada() {

    return  detalleClienteRepository.findAll();
  }



}
